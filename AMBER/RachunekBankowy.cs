﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER.Interfaces;
using AMBER.Impl;

namespace AMBER
{
    public class RachunekBankowy
    {
        private List<RachunekBankowyModel> rbmList;
        private IRachunek rachunek;
        public RachunekBankowy() 
        {
            Rachunek r = new Rachunek();
            rachunek = r;
            rbmList = rachunek.GetBankAccountList();
        }
        public RachunekBankowyModel MojRachunekBankowy(string UserName)
        {
            return rbmList.Where(x => x.UserName == UserName).Single();
        }
    }

    public class RachunekBankowyModel
    {
        public RachunekBankowyModel() {}

        public int Id { get; set; }
        public string UserName { get; set; }

        public Saldo Saldo { get; set; }
        public OperacjaBankowa OperacjaBankowa { get; set; }
        public List<Raport> Raport { get; set; }
        public Historia Historia { get; set; }
    }
}
