﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMBER
{
    public class OperacjaBankowa
    {
        private RachunekBankowyModel rbm;
        public OperacjaBankowa(RachunekBankowy rb, string userName) 
        {
            rbm = rb.MojRachunekBankowy(userName);
        }

        public RachunekBankowyModel WyswietlRachunek()
        {
            return rbm;
        }

        private void ChangeAccountState()
        {

        }

        private string ChangeUserName(string userName)
        {
            return userName;
        }
    }
}
