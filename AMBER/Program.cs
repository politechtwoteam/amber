﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMBER
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("AMBER");
            string userName = Console.ReadLine();
            var rb = new RachunekBankowy();
            
            var opbn = new OperacjaBankowa(rb, userName);
            var rbm = opbn.WyswietlRachunek();

            Console.WriteLine(rbm.Saldo.ZwrocStan());

            Console.ReadKey();
        }
    }
}
