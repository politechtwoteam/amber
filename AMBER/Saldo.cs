﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMBER
{
    public class Saldo
    {
        public Saldo()
        {

        }

        public int Id { get; set; }
        public RachunekBankowyModel RachunekBankowyModel { get; set; }

        private float Stan { get; set; }
        private float Odsetki { get; set; }
        private bool IsOdsetki { get; set; }
        private float Debet { get; set; }
        private bool IsDebit { get; set; }

        public float ZwrocStan()
        {
            return Stan;
        }
        public void ZmienStan(bool isAdd, float cash)
        {
            Stan += cash;
        }
    }
}
