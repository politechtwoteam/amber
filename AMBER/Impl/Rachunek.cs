﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER.Interfaces;

namespace AMBER.Impl
{
    public class Rachunek : IRachunek
    {
        public List<RachunekBankowyModel> GetBankAccountList()
        {
            var list = new List<RachunekBankowyModel>();
            list.Add(new RachunekBankowyModel()
            {
                Id = 1,
                Historia = null,
                OperacjaBankowa = null,
                Raport = null,
                Saldo = new Saldo(),
                UserName = "Jaś1"
            });
            list.Add(new RachunekBankowyModel() 
            { 
                Id = 2, 
                Historia = null, 
                OperacjaBankowa = null, 
                Raport = null, 
                Saldo = new Saldo(), 
                UserName = "Jaś2" 
            });
            list.Add(new RachunekBankowyModel() 
            { 
                Id = 3, 
                Historia = null, 
                OperacjaBankowa = null, 
                Raport = null, 
                Saldo = new Saldo(), 
                UserName = "Jaś3" 
            });

            list[0].Saldo.ZmienStan(true, 200002.50f);
            list[1].Saldo.ZmienStan(true, 123.10f);
            list[2].Saldo.ZmienStan(true, 241245.23f);

            return list;
        }
    }
}
